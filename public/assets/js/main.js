/* 
 * ***************************************************************
 * Script : main.js
 * Version : 
 * Date : Feb 22, 2018 - 2:13:39 PM
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
var ajax_g_proces;
setInterval(function () {
    var tanggal = moment().format('DD MMMM YYYY, H:mm:ss');
    $(".date-time").html(tanggal);
}, 1000);


$(".nav-menu").click(function (event) {
    event.preventDefault();
    var url = $(this).attr('href');

    var title = $(this).html();
    var desc = $(this).attr('title');
    $(".breadcrumb-title").html(title + ' <small>' + desc + '</small>');

    $(".nav-menu").removeClass("active");
    $(this).addClass("active");
    var app = $(".app").hasClass("sidebar-mobile-show");
    if (app) {
        $(".app").removeClass("sidebar-mobile-show");
    }
    localStorage.url = url;
    localStorage.title = title;
    localStorage.desc = desc;
    if(ajax_g_proces){
        ajax_g_proces.abort();
    }
    load_url(url, title, desc);
});

if (localStorage.url) {
    var url = localStorage.url;
    var title = localStorage.title;
    var desc = localStorage.desc;
    load_url(url, title, desc);
}

function load_url(url, title, desc) {
    var n = title.indexOf("</i>");
    var card_title = title.substring(n, 50);
    ajax_g_proces = $.ajax({
        type: "GET",
        url: url,
        beforeSend: function () {
            $("#main-content").html("");
            $('body').removeClass('loaded');
        },
        success: function (resp) {
            $('body').addClass('loaded');
            if (!resp) {
                localStorage.removeItem("url");
                localStorage.removeItem("title");
                localStorage.removeItem("desc");
                //location.reload();
            }
            $("#main-content").html(resp);
            $(".card-title").html(card_title + ' <small>' + desc + '</small>');
            set_controls();
        },
        error: function (event, textStatus, errorThrown) {
            $('body').addClass('loaded');
            console.log('Pesan: ' + textStatus + ' , HTTP: ' + errorThrown);
        }
    });
}

$(".nav-main-menu").click(function (event) {
    $(".nav-dropdown").removeClass("open");
});

$(".nav-single-menu").click(function (event) {
    $(".nav-dropdown").removeClass("open");
});

$(".btn-logout").click(function () {
    logout();
});

function logout() {
    localStorage.url = base_url('main/home/');
    localStorage.title = "Dashboard";
    localStorage.desc = "Dashboard";
    location.replace(base_url('access/logout/'));
}


$(".chosen-select").chosen({
    no_results_text: "Maaf, data tidak ditemukan!"
});

$('.year').datepicker({
    startView: "year",
    minViewMode: "years",
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: false,
    autoclose: true,
    format: "yyyy"
});

$('.calendar').datepicker({
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: false,
    autoclose: true,
    format: "dd-mm-yyyy"
});

$('.month').datepicker({
    startView: "year",
    minViewMode: "months",
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: false,
    autoclose: true,
    format: "mm-yyyy"
});

$('#form-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var title = button.data('title');
    var action_url = button.data('action-url');
    var post_id = button.data('post-id');
    var width = button.data('width');
    if (width) {
//        console.log(width);
        $(".modal-dialog").css("max-width", width);
    }
    if (action_url !== undefined) {
        $.ajax({
            type: "GET",
            url: base_url(action_url),
            data: {"id": post_id},
            beforeSend: function () {
                $("#form-modal-content").html("");
            },
            success: function (resp) {
                $("#form-modal-content").html(resp);
                set_controls();
            },
            error: function (event, textStatus, errorThrown) {
                swal("Kesalahan!", 'Pesan: ' + textStatus + ' , HTTP: ' + errorThrown, "error");
            }
        });
        var modal = $(this);
        modal.find('.modal-title').text(title);
    }
});

$('#form-modal').on('hidden.bs.modal', function () {
    $(".modal-dialog").css("max-width", "");
    $("#form-modal-content").html("");
});

$('#form-modal').on('shown.bs.modal', function () {
    $('.chosen-select', this).chosen('destroy').chosen({
        no_results_text: "Maaf, data tidak ditemukan!"
    });
});

function set_controls() {
    $(".chosen-select").chosen({
        no_results_text: "Maaf, data tidak ditemukan!"
    });

    $('.year').datepicker({
        startView: "year",
        minViewMode: "years",
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy"
    });

    $('.calendar').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "dd-mm-yyyy"
    });

    $('.month').datepicker({
        startView: "year",
        minViewMode: "months",
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "mm-yyyy"
    });
}

function update_csrf(param) {
    $("meta[name=csrf]").attr("content", param.csrf_return);
    $("input[name^='csrf']").val(param.csrf_return);
    $("hidden[name^='csrf']").val(param.csrf_return);
}

/*
 * Datatable Generator
 * 
 * element = Class HTML Document Object Model (DOM) 
 * url = Url Destination
 * column_list = Column list for datasource 
 * ex : 
 * var column_list = [
 {"data": "menu_name",
 render: $.fn.dataTable.render.text()
 },
 {"data": "submenu",
 render: $.fn.dataTable.render.text()
 },
 {"data": "description",
 render: $.fn.dataTable.render.text()
 },
 {"data": "id",
 render: function (data, type, row) {
 var btn = '<a class="btn btn-outline-primary btn-sm"' + 
 ' data-toggle="modal"' + 
 ' data-title="Edit Data"' + 
 ' data-post-id="'+data+'"' +
 ' data-action-url="menus/form"' +
 ' data-target="#form-modal"' +
 ' href="javascript:void(0);">' + 
 '<i class="fa fa-pencil"></i></a>' + 
 '' + 
 '<a class="btn btn-outline-danger btn-sm"' + 
 ' onclick="deleted(\''+data+'\');"' +
 ' href="javascript:void(0);">' + 
 '<i class="fa fa-trash"></i></a>';
 return btn;
 }
 }
 ];
 * column_def = Column Definition for custom column 
 * ex :
 * var column_def = [
 {"orderable": false, "targets": 3}
 ];
 * data_push = Extra data if you need for more variable
 * ex : 
 * var data_push = [
 { "name": "kelas", "value": 1 }
 ,{ "name": "id_tahun_akademik", "value": 2018 }
 ];
 */

// Variable global untuk datatable
var glob_where_datatable;
function set_datatable(element, url, column_list, column_def, data_push, order) {
    table = $('.' + element).DataTable({
        "bProcessing": true,
        "bServerSide": true,
        "columns": column_list,
        "columnDefs": column_def,
        "order": order,
        "lengthMenu": [[10, 25, -1], [10, 25, "All"]],
        "pagingType": "full_numbers",
        "fnServerParams": function (aoData) {
            aoData.push({"name": $('meta[name=csrf]').attr("id")
                , "value": $('meta[name=csrf]').attr("content")});
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            if (glob_where_datatable) {
                for (i = 0; i < glob_where_datatable.length; i++) {
                    aoData.push(glob_where_datatable[i]);
                }
            }
            if (data_push) {
                for (i = 0; i < data_push.length; i++) {
                    aoData.push(data_push[i]);
                }
            }
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": sSource,
                "data": aoData,
                "success": function (resp) {
                    update_csrf(resp);
                    //$('meta[name=csrf]').attr("content",resp.csrf_return);
                    fnCallback(resp);
                },
                "error": function (event, textStatus, errorThrown) {
                    swal({
                        title: "Kesalahan!",
                        html: 'Pesan: Tidak dapat menerima token, halaman akan di reload',
                        type: "info"
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });
                }
            });
        },
        "sAjaxSource": url,
        buttons: [
            {extend: 'copy',
                exportOptions: {orthogonal: 'export'}},
            {extend: 'excel',
                exportOptions: {orthogonal: 'export'}}
        ],
        "sDom": "<'row'<'col-sm-6'B><'col-sm-6 text-right' l> r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
        //"sDom": "<'row'<'col-sm-6'l><'col-sm-6 text-right'>r> t <'row'<'col-sm-6'i><'col-sm-6 text-right'p>> ",
        "oLanguage": {
            "sLengthMenu": "_MENU_",
            "sZeroRecords": "Maaf, data yang anda cari tidak ditemukan",
            "sProcessing": "<i class=\"fa fa-refresh fa-spin\"></i> Silahkan Tunggu ...",
            "sInfo": "_START_-_END_ / _TOTAL_",
            "sInfoEmpty": "0-0 / 0",
            "infoFiltered": "(_MAX_)",
            "oPaginate": {
                "sPrevious": "<i class='fa fa-angle-double-left'></i>",
                "sNext": "<i class='fa fa-angle-double-right'></i>"
            }
        }
    });

    $('.' + element).tooltip({
        selector: "[data-toggle=tooltip]",
        container: "body"
    });

    $('.' + element + ' tfoot th').each(function () {
        var title = $('.' + element + '  tfoot th').eq($(this).index()).text();
        if (title !== "Aksi") {
            $(this).html('<input type="text" class="form-control form-control-sm form-datatable" style="width:100%;border-radius: 0px;" placeholder="Cari ' + title + '" />');
        } else {
            $(this).html('');
        }
    });

    table.columns().every(function () {
        var that = this;
        $('input', this.footer()).on('keyup change', function (ev) {
            //if (ev.keyCode == 13) { //only on enter keypress (code 13)
            that
                    .search(this.value)
                    .draw();
            //}
        });
    });
}