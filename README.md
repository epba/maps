# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Gmaps - Web Application
* (Map Manipulation by Google Maps)
* Version Beta 1.0
* Author: 	Pudyasto Adi Wibowo
* Website: 	http://www.pudyastoadi.web.id/
* Contact: 	mr.pudyasto@gmail.com
* Follow: 	https://twitter.com/pudyastoadi
* Like: 	https://www.facebook.com/dhyaz.cs
* License: 	http://opensource.org/licenses/MIT	MIT License

### How do I get set up? ###

* Please config your virtual host into folder /public

* Try your application

* Username And Password
    - Username : admin
    - Password : admin

### Special Thanks to ###

* CoreUI - https://github.com/coreui/coreui-free-bootstrap-admin-template
* Codeigniter 3 - https://codeigniter.com/
* HMVC - https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
* Template Engine - https://github.com/philsturgeon/codeigniter-template
* Google Maps - https://developers.google.com/maps/?hl=id
* Titounnes (Menu Recursive) - https://github.com/titounnes/menu-recursive

### Support Us ###
* Donate via BCA - 2460637638

Happy Coding