<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Debug
 *
 * @author adi
 */
class Debug extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => "Error",
            'msg_detail' => " Page",
            'menu_app' => $this->menu_app,
        );
    }

    //redirect if needed, otherwise display the user list
    
    public function index()
    {
        $this->load->view('err_505', $this->data, false);
    } 
    
    public function err_505()
    {
        $this->load->view('err_505', $this->data, false);
    }
    
    public function err_404()
    {
        $this->load->view('err_404', $this->data, false);
    }
    
    public function err_500()
    {
        $this->load->view('err_500', $this->data, false);
    }
}
