<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">			
    <div class="row">
        <div class="col-sm-12">
            <div class="text-center error-box">
                <h1 class="error-text tada animated"><i class="fa fa-times-circle text-danger error-icon-shadow"></i> Error 500</h1>
                <h2 class="font-xl"><strong>Server Error</strong></h2>
                <br />
                <p class="lead">
                    Server sedang mengalami masalah!
                    <br>
                    Silahkan tunggu dan coba beberapa saat lagi, atau Silahkan hubungi IT Administrator Anda.
                </p>
            </div>
        </div>
    </div>
</div>