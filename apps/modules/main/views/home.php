<style>
  /* Always set the map height explicitly to define the size of the div
   * element that contains the map. */
  #map {
    height : 100%; 
    width : 100%; 
    top : 0; 
    left : 0; 
    position : absolute; 
    z-index : 200;
  }
  /* Optional: Makes the sample page fill the window. */
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
</style>
<div id="map"></div>
<script>
    var map;
    var arrArea = [];
    var SetArea;  
    var infoWindow;
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
          center: {lat: -7.2633851, lng: 110.3694903},
          mapTypeId: 'roadmap',
          disableDoubleClickZoom: true,
      });

      getArea();
    }
    
    function getArea(){
        $.ajax({
            type: "get",
            url: "<?=site_url('main/getarea');?>",
            data: {"command":"show"},
            success: function(resp){   
                var obj = jQuery.parseJSON(resp);
                $.each(obj, function(key, value){
                    var myJsonString = JSON.parse(value.area);
                    SetArea = new google.maps.Polygon({
                      paths: myJsonString,
                      strokeColor: '#000',
                      strokeOpacity: 0.8,
                      strokeWeight: 1,
                      fillColor: value.color,
                      fillOpacity: 0.5
                    });  
                    SetArea.setMap(map); 
                    infoWindow = new google.maps.InfoWindow;
                    google.maps.event.addListener(SetArea, 'click', function(e) {
                        infoWindow.setPosition(e.latLng);
                        showDetail(value);
                    });
                });               
            },
            error:function(event, textStatus, errorThrown) {
                swal("Error !", 'Error Message: ' + textStatus + ' , HTTP Error: ' + errorThrown, "error");
            }
        });   
  }
  
    function showDetail(data){
        infoWindow.setContent(templateContent(data));
        infoWindow.open(map);
    }
    
    function templateContent(data){
        var myJsonString = JSON.parse(data.area);
        var arrArea = [];
        $.each(myJsonString, function(key, value){
            var param = new google.maps.LatLng(value.lat, value.lng);
            // console.log(param);
            arrArea.push(param);            
            // console.log(value.lat + ' - ' + value.lng);
        });
        var area = google.maps.geometry.spherical.computeArea(arrArea);
        // console.log(area);
        var popup = '<b>Wilayah '+data.name+' </b><br><hr>' +
          'Luas Wilayah : <br> ' + numeral(area).format('0,0.000') + ' Meter' + 
          '<br><br>' +
          'Keterangan : <br> ' + data.description ;
        return popup;
    }
    initMap();
</script>