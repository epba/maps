<div class="animated fadeIn">
    <div class="row">
    <div class="col-md-12">
    </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-9">
                    <div id="map"></div>
                </div>
                <div class="col-lg-3">
                    
                    <div class="alert alert-info">
                        <strong>Cara Pengisian</strong> 
                        <ol>
                            <li>Pilih Warna</li>
                            <li>Masukkan Nama Wilayah</li>
                            <li>Masukkan Deskripsi</li>
                            <li><b><i>Double click</i></b> pada peta untuk membuat titik koordinat</li>
                            <li>Lanjutakn langkah ke 4 untuk menciptakan sebuah polygon pada map (Minimal 3 titik)</li>
                            <li>Setelah selesai klik simpan</li>
                        </ol>
                    </div>
                    <?php
                        $attributes = array(
                            'role' => 'form'
                            , 'id' => 'form_add'
                            , 'name' => 'form_add');
                        echo form_open('polygon/submit',$attributes);
                    ?>
                    <div class="form-group">
                        <?php 
                            echo form_input($form['id']);
                            echo form_label($form['color']['placeholder']);
                            echo form_input($form['color']);
                            echo form_error('color','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo form_label($form['name']['placeholder']);
                            echo form_input($form['name']);
                            echo form_error('name','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="form-group">
                        <?php 
                            echo form_label($form['description']['placeholder']);
                            echo form_textarea($form['description']);
                            echo form_error('description','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="from-group">
                        <?php 
                            echo form_label($form['area']['placeholder']);
                            echo form_textarea($form['area']);
                            echo form_error('area','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <hr>
                    <div class="from-group">
                        <button type="submit" class="btn btn-primary btn-simpan">Simpan Data</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Batal</button>
                    </div>
                    <?=form_close();?>
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
</div>

<!-- ColorPicker -->
<link rel="stylesheet" href="<?=base_url('assets/plugins/colorpicker/bootstrap-colorpicker.min.css');?>">
<script src="<?=base_url('assets/plugins/colorpicker/bootstrap-colorpicker.min.js');?>"></script>
<script>
    var map;
    var arrArea = [];
    var SetArea; 
    var deleteMenu;
    var infoWindow;
    $('.my-colorpicker1').colorpicker();
    $('.btn-reset').click(function(){
        location.reload();
    });
    $('.btn-simpan').click(function(){
        // saveData();
    });

        $("#form_add").on( "submit", function( event ) {
            event.preventDefault();
            var input = $( this ).serialize();
            var submit = $( this ).attr('action');
            $.ajax({
                type: "POST",
                url: submit,
                data: input,
                success: function(resp){   
                    // console.log(resp);
                    var obj = jQuery.parseJSON(resp);
                    update_csrf(obj);
                    if(obj.state==="1"){
                        swal({
                            title: obj.title,
                            html: obj.msg,
                            type: "success"
                        }).then((result) => {
                            if (result.value) {
                                table.ajax.reload();
                                $('#form-modal').modal("hide");
                            }
                        });    
                    }else{
                        swal({
                            title: obj.title,
                            html: obj.msg,
                            type: "error"
                        }).then((result) => {
                            if (result.value) {
                                
                            }
                        });    
                    }
                },
                error:function(event, textStatus, errorThrown) {
                    swal({
                        title: "Kesalahan!",
                        html: 'Pesan: ' + textStatus + ' , HTTP: ' + errorThrown,
                        type: "error"
                    }).then((result) => {
                        if (result.value) {
                            //location.reload();
                        }
                    }); 
                }
            });
        });
    
    function initMap() {
        var defaultPos = {lat: -7.3502493, lng: 110.1086289};
        if($("#id").val()){
            var result = $("#area").val();
            var myJsonString = JSON.parse(result);
            defaultPos = myJsonString[0];
        }
        // Inisialisasi map google
        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: defaultPos,
            mapTypeId: 'roadmap',
            disableDoubleClickZoom: true,
        });

        
        google.maps.event.addListener(map, 'dblclick', function( event ){
            // Event double click pada map akan menghasilkan vertex (Titik Koordinat)
            // Kemudian akan di olah pada fungsi getArea
            var name = $("#name").val();
            var area = $("#area").val();
            var description = $("#description").val();

            if(name==""){
                alert("Masukkan nama wiayah dan keterangan");
                return;
            }else{
                getArea(event.latLng.lat(),event.latLng.lng());
            }
        });
        

        if($("#id").val()){
            showArea();
        }
      //getKabArea();
    }

    function getArea(vlat,vlng){
        // Mengambil data koordinat Lat - Long
        var label = {
              lat: vlat,
              lng: vlng
          };
        // Mengolah data-data koordinat dengan memasukkan kedalam Array (arrArea)
        arrArea.push(label);
        var obj = JSON.stringify(arrArea);

        // Memasukkan data-data koordinat kedalam textarea id=area
        $("#area").val(obj);
        if(SetArea){
            SetArea.setMap(null); 
        }

        // Memanggil fungsi menampilkan area
        showArea();
    }

    function showArea(){
        var result = $("#area").val();
        var myJsonString = JSON.parse(result);
        // Menampilkan polygon dari textarea id=area
        var color = $("#color").val();
        if(color===""){
            color = "#ff0000";
        }
          SetArea = new google.maps.Polygon({
            paths: myJsonString,
            strokeColor: '#000',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: color,
            fillOpacity: 0.5,
            editable: true,
            draggable: true,
          });  
          SetArea.setMap(map);    
          SetArea.addListener('click', showArrays);
          SetArea.addListener('dragend', updatePosition);
          SetArea.addListener('mousemove', updatePosition);
          
          if(!deleteMenu){
              deleteMenu = new DeleteMenu();
          }
          infoWindow = new google.maps.InfoWindow;

          google.maps.event.addListener(SetArea, 'rightclick', function(e) {
            // Event klik kanan pada mouse, jika tepat pada vertex (titik koordinat maka akan muncul menu delete vertex)
            if (e.vertex == undefined) {
              return;
            }
            if(deleteMenu){
                deleteMenu.open(map, SetArea.getPath(), e.vertex);
            }
          });
    }
  
    function showArrays(event) {
        // Menampilkan popup informasi area / wilayah yang terpilih
      var vertices = this.getPath();

        var name = $("#name").val();
        var area = $("#area").val();
        var description = $("#description").val();

        if(name==""){
            alert("Masukkan nama wiayah dan keterangan");
            return;
        }

      var contentString = '<b>Wilayah '+name+' </b><br><hr>' +
          'Keterangan : <br> ' + description + 
          '<br><br><br>' + 
          'Koordinat : <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
          '<br>';
      var arrFinalArea = [];
      arrArea = [];
      for (var i =0; i < vertices.getLength(); i++) {
        var xy = vertices.getAt(i);
        var label = {
              lat: xy.lat(),
              lng: xy.lng()
          };
        arrFinalArea.push(label);
        arrArea.push(label);
        var obj = JSON.stringify(arrFinalArea);
        $("#area").val(obj);
        
        // contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' + xy.lng();
      }
      infoWindow.setContent(contentString);
      infoWindow.setPosition(event.latLng);

      infoWindow.open(map);
    }

    function updatePosition(event) {
        // Mengupdate posisi area polygon   
      var vertices = this.getPath();
      //console.log(vertices.getAt);
      var arrFinalArea = [];
      arrArea = [];
      for (var i =0; i < vertices.getLength(); i++) {
        var xy = vertices.getAt(i);
        var label = {
              lat: xy.lat(),
              lng: xy.lng()
          };
        arrFinalArea.push(label);
        arrArea.push(label);
        var obj = JSON.stringify(arrFinalArea);
        $("#area").val(obj);
      }
    }

    function DeleteMenu() {
        // Menampilkan menu delete
      this.div_ = document.createElement('div');
      this.div_.className = 'delete-menu';
      this.div_.innerHTML = 'Delete';

      var menu = this;
      google.maps.event.addDomListener(this.div_, 'click', function() {
        // Event klik hapus pada vertex (Titik Koordinat)
        menu.removeVertex();
      });
    }

    DeleteMenu.prototype = new google.maps.OverlayView();

    DeleteMenu.prototype.onAdd = function() {
      var deleteMenu = this;
      var map = this.getMap();
      this.getPanes().floatPane.appendChild(this.div_);

      this.divListener_ = google.maps.event.addDomListener(map.getDiv(), 'mousedown', function(e) {
        if (e.target != deleteMenu.div_) {
          deleteMenu.close();
        }
      }, true);
    };

    DeleteMenu.prototype.onRemove = function() {
      google.maps.event.removeListener(this.divListener_);
      this.div_.parentNode.removeChild(this.div_);

      // clean up
      this.set('position');
      this.set('path');
      this.set('vertex');
    };

    DeleteMenu.prototype.close = function() {
      this.setMap(null);
    };

    DeleteMenu.prototype.draw = function() {
      var position = this.get('position');
      var projection = this.getProjection();

      if (!position || !projection) {
        return;
      }

      var point = projection.fromLatLngToDivPixel(position);
      this.div_.style.top = point.y + 'px';
      this.div_.style.left = point.x + 'px';
    };

    DeleteMenu.prototype.open = function(map, path, vertex) {
      this.set('position', path.getAt(vertex));
      this.set('path', path);
      this.set('vertex', vertex);
      this.setMap(map);
      this.draw();
    };

    DeleteMenu.prototype.removeVertex = function() {
      var path = this.get('path');
      var vertex = this.get('vertex');

      if (!path || vertex == undefined) {
        this.close();
        return;
      }

      path.removeAt(vertex);
      this.close();
      var vertices = SetArea.getPath();
      var arrFinalArea = [];
      if(vertices.getLength()===0){
          $("#area").val("");
          arrArea = [];
      }
      for (var i =0; i < vertices.getLength(); i++) {
        var xy = vertices.getAt(i);
        var label = {
              lat: xy.lat(),
              lng: xy.lng()
          };
        arrFinalArea.push(label);
        arrArea.push(label);
        var obj = JSON.stringify(arrFinalArea);
        $("#area").val(obj);
      }
    };

    initMap();
    // google.maps.event.addDomListener(window, 'load', initMap);
</script>