<?php
/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */
?>
<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="card-title">-</span>
                    <div class="card-actions ">
                        <a href="# " class="btn-refersh" ><i class="icon-reload"></i></a>
                    </div>
                </div>            
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <a class="btn btn-outline-primary" href="#" data-toggle="modal"
                               data-title="Tambah Data" data-post-id=""
                               data-action-url="polygon/form"
                               data-width="90%"
                               data-target="#form-modal"><i class="fa fa-plus-circle"></i> Tambah</a>
                            <a href="javascript:void(0);" class="btn btn-outline-secondary btn-refersh">Refresh</a>

                            <div class="table-responsive">
                                <table class="table table-responsive-sm table-bordered table-hover table-sm dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width: 200px;text-align: center;">Nama Area</th>
                                            <th style="width: 200px;text-align: center;">Warna</th>
                                            <th style="text-align: center;">Keterangan</th>
                                            <th style="min-width: 50px;width: 50px;text-align: center;">
                                                <i class="fa fa-th"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nama Area</th>
                                            <th>Warna</th>
                                            <th>Keterangan</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody></tbody>
                                </table> 
                            </div>                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.col-->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-refersh").click(function () {
            table.ajax.reload();
        });

        var column_list = [
            {"data": "name",
                render: $.fn.dataTable.render.text()
            },
            {"data": "color",
                render: $.fn.dataTable.render.text()
            },
            {"data": "description",
                render: $.fn.dataTable.render.text()
            },
            {"data": "id",
                render: function (data, type, row) {
                    var btn = '<center>' + 
                            '<a class="btn btn-outline-primary btn-sm"' +
                            ' data-toggle="modal"' +
                            ' data-title="Edit Data"' +
                            ' data-post-id="' + data + '"' +
                            ' data-width="90%"' + 
                            ' data-action-url="polygon/form"' +
                            ' data-target="#form-modal"' +
                            ' href="javascript:void(0);">' +
                            '<i class="fa fa-pencil"></i>' + 
                            '</a>' +
                            ' / ' +
                            '<button class="btn btn-outline-danger btn-sm"' +
                            ' onclick="deleted(\'' + data + '\');"' +
                            ' type="button">' +
                            '<i class="fa fa-trash"></i>' + 
                            '</button>'+
                            '</center>';
                    return btn;
                }
            }
        ];

        var column_def = [
            {
                "targets": [ 0 ],
                "orderData": [ 0, 1 ]
            }, 
            {
                "orderable": false, 
                "targets": 3
            }
        ];
        
        set_datatable('dataTable', "<?= site_url('polygon/json_dgview'); ?>", column_list, column_def);
    });

    function deleted(id) {
        swal({
            title: "Konfirmasi Hapus",
            text: "Data yang dihapus, tidak dapat dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Lanjutkan",
            cancelButtonText: "Tidak, Batalkan"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "<?=site_url('polygon/submit')?>",
                    data: {"id": id
                            , "stat": "delete"
                            , "<?= $this->security->get_csrf_token_name(); ?>" : $('meta[name=csrf]').attr("content")
                            },
                    success: function (resp) {
                        var obj = jQuery.parseJSON(resp);
                        update_csrf(obj);
                        if (obj.state === "1") {
                            swal({
                                title: obj.title,
                                html: obj.msg,
                                type: "success"
                            }).then((result) => {
                                if (result.value) {
                                    table.ajax.reload();
                                }
                            }); 
                        } else {
                            swal({
                                title: obj.title,
                                html: obj.msg,
                                type: "error"
                            }).then((result) => {
                                if (result.value) {
                                    
                                }
                            });  
                        }
                    },
                    error: function (event, textStatus, errorThrown) {
                        swal({
                            title: "Kesalahan!",
                            html: 'Pesan: ' + textStatus + ' , HTTP: ' + errorThrown,
                            type: "error"
                        }).then((result) => {
                            if (result.value) {
                                //location.reload();
                            }
                        }); 
                    }
                });
            }
        });
    }
</script>