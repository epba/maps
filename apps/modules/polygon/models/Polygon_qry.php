<?php

/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Polygon_qry
 *
 * @author adi
 */
class Polygon_qry extends CI_Model{
    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function select_data($param) {
        $this->db->where('id', $param);
        $query = $this->db->get('maps');
        return $query->result_array();
    }

    public function submit() {
        try {
            $array = $this->input->post();
            if (empty($array['id'])) {
                unset($array['id']);
                $resl = $this->db->insert('maps', $array);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->title = "Kesalahan";
                    $this->msg = " Kesalahan : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->title = "Berhasil";
                    $this->msg = "Data Berhasil Disimpan";
                    $this->state = "1";
                }
            } elseif (!empty($array['id']) && empty($array['stat'])) {
                $this->db->where('id', $array['id']);
                $resl = $this->db->update('maps', $array);
                if (!$resl) {
                    $err = $this->db->error();
                    $this->title = "Kesalahan";
                    $this->msg = " Kesalahan : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->title = "Berhasil";
                    $this->msg = "Data Berhasil Diupdate";
                    $this->state = "1";
                }
            } elseif (!empty($array['id']) && !empty($array['stat'])) {
                $this->db->where('id', $array['id']);
                $resl = $this->db->delete('maps');
                if (!$resl) {
                    $err = $this->db->error();
                    $this->title = "Kesalahan";
                    $this->msg = " Kesalahan : " . $this->apps->err_code($err['message']);
                    $this->state = "0";
                } else {
                    $this->title = "Berhasil";
                    $this->msg = "Data Berhasil Dihapus";
                    $this->state = "1";
                }
            } else {
                $this->title = "Kesalahan";
                $this->msg = "Variabel Tidak Sesuai";
                $this->state = "0";
            }
        } catch (Exception $e) {
            $this->msg = $e->getMessage();
            $this->state = "0";
        }
        
        $arr = array(
            'title' => $this->title,
            'msg' => $this->msg,
            'state' => $this->state,
            'csrf_return' => $this->security->get_csrf_hash(),
        );
        return json_encode($arr);
    }
}
