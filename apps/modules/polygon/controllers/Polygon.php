<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * ***************************************************************
 *  Script : 
 *  Version : 
 *  Date :
 *  Author : Pudyasto Adi W.
 *  Email : mr.pudyasto@gmail.com
 *  Description : 
 * ***************************************************************
 */

/**
 * Description of Polygon
 *
 * @author adi
 */
class Polygon extends MY_Controller {
    protected $data = '';
    public function __construct()
    {
        parent::__construct();
        $this->data = array(
            'msg_main' => "Maps Polygon",
            'msg_detail' => "Maps Polygon",
        );
        $this->load->model('polygon_qry');
    }

    //redirect if needed, otherwise display the user list
    
    public function index(){  
        $this->load->view('index', $this->data, false);
    }   

    public function form(){  
        $id = $this->input->get('id');
        if($id){
            $this->_init_edit($id);
        }else{
            $this->_init_add();
        }
        echo $this->load->view('form',$this->data,false);
    }     
    
    public function json_dgview() {
        $columns = array( 'name', 'description', 'color','id', );
        $table = 'maps';
        $index = "id";
        $output = $this->paw_table->output($columns, $table, $index);
        echo $output;
    }
    
    private function _init_add(){
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Pilih Warna',
                    'id'          => 'id',
                    'name'        => 'id',
                    'value'       => set_value('id'),
                    'class'       => 'form-control',
            ),
            'name'=> array(
                     'placeholder' => 'Nama Wilayah',
                     'id'          => 'name',
                     'name'        => 'name',
                     'value'       => set_value('name'),
                     'class'       => 'form-control',
                     'required'    => '',
             ),
             'description'=> array(
                      'placeholder' => 'Keterangan Wilayah',
                      'id'          => 'description',
                      'name'        => 'description',
                      'value'       => set_value('description'),
                      'class'       => 'form-control',
                      'style'       => 'resize: vertical;min-height: 100px;height: 120px;',
              ),
           'color'=> array(
                    'placeholder' => 'Pilih Warna',
                    'id'          => 'color',
                    'name'        => 'color',
                    'value'       => '#ff0000',
                    'class'       => 'form-control my-colorpicker1',
                    'required'    => '',
            ),
            'area'=> array(
                    'placeholder' => 'Data Area',
                    'id'          => 'area',
                    'name'        => 'area',
                    'value'       => set_value('area'),
                    'class'       => 'form-control',
                    'style'       => 'resize: vertical;min-height: 100px;height: 120px;',
                    'readonly'    => '',
            ),
        );
    }

    
    
    private function _init_edit($id){
        if(empty($id)){
            return false;
        }
        
        $this->val = $this->polygon_qry->select_data($id);
        
        if(empty($this->val)){
            return false;
        }
        $this->data['form'] = array(
           'id'=> array(
                    'type'        => 'hidden',
                    'placeholder' => 'Pilih Warna',
                    'id'          => 'id',
                    'name'        => 'id',
                    'value'       => $this->val[0]['id'],
                    'class'       => 'form-control',
            ),
            'name'=> array(
                     'placeholder' => 'Nama Wilayah',
                     'id'          => 'name',
                     'name'        => 'name',
                     'value'       => $this->val[0]['name'],
                     'class'       => 'form-control',
                     'required'    => '',
             ),
             'description'=> array(
                      'placeholder' => 'Keterangan Wilayah',
                      'id'          => 'description',
                      'name'        => 'description',
                      'value'       => $this->val[0]['description'],
                      'class'       => 'form-control',
                      'style'       => 'resize: vertical;min-height: 100px;height: 120px;',
              ),
           'color'=> array(
                    'placeholder' => 'Pilih Warna',
                    'id'          => 'color',
                    'name'        => 'color',
                    'value'       => $this->val[0]['color'],
                    'class'       => 'form-control my-colorpicker1',
                    'required'    => '',
            ),
            'area'=> array(
                    'placeholder' => 'Data Area',
                    'id'          => 'area',
                    'name'        => 'area',
                    'value'       => $this->val[0]['area'],
                    'class'       => 'form-control',
                    'style'       => 'resize: vertical;min-height: 100px;height: 120px;',
                    'readonly'    => '',
            ),
        );
    }
    
    public function submit() {  
        $id = $this->input->post('id');
        $stat = $this->input->post('stat');
        if($this->_validate($id,$stat) == TRUE){
            $res = $this->polygon_qry->submit();
            echo $res;
        }else{
            $arr = array(
                'state' => 0, 
                'msg' => "Data tidak lengkap",
                'csrf_return' => $this->security->get_csrf_hash(),
            );
            echo json_encode($arr);
        }
    }
    
    private function _validate($id,$stat) {
        if(!empty($id) && !empty($stat)){
            return true;
        }
        $config = array(
            array(
                    'field' => 'color',
                    'label' => 'Warna',
                    'rules' => 'required|max_length[7]',
                    ),
            array(
                    'field' => 'name',
                    'label' => 'Wilayah',
                    'rules' => 'required|max_length[50]',
                    ),
        );
        
        $this->form_validation->set_rules($config);   
        if ($this->form_validation->run() == FALSE)
        {
            return false;
        }else{
            return true;
        }
    }
}
