<div class="row justify-content-center">
<div class="col-md-12 text-center m-3">
        <img src="<?=base_url('assets/img/maps.png');?>" style="max-height: 150px;padding-bottom: 10px;">
        <h3 style="font-size: 35px;
    text-align: center;
    margin-bottom: 25px;
    font-weight: 300;">
            Google Maps Apps
            </h3>
        <p style="font-size: 18px;"><i>“Powered by Google”</i></p>
    </div>
    <div class="col-md-5">
        <div class="card-group">
            <div class="card p-12">
                <?php
                    $attributes = array(
                        'id' => 'form_login'
                        , 'name' => 'form_login');
                    echo form_open('access/login',$attributes); 
                ?>
                <div class="card-body">
                    <h1>Login</h1>
                    <p class="text-muted">Sign In to your account</p>
                    <div class="input-group mb-3">
                        <span class="input-group-addon"><i class="icon-user"></i></span>
                        <?php 
                            echo form_input($form['username']);
                            echo form_error('username','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="input-group mb-4">
                        <span class="input-group-addon"><i class="icon-lock"></i></span>
                        <?php 
                            echo form_input($form['password']);
                            echo form_error('password','<div class="note">','</div>'); 
                        ?>
                    </div>
                    <div class="row">
                    <div class="col-12 text-center">
                        Username : admin
                        <br>
                        Password : admin
                    </div>
                        <div class="col-6">
                            <button type="submit" value="login" name="submit" class="btn btn-primary px-4">Login</button>
                        </div>
                        <div class="col-6 text-right">
                            <button type="button" class="btn btn-link px-0 btn-reset-password">Forgot password?</button>
                        </div>
                    </div>
                    <p class="text-muted text-center text-footer">
                        <?php echo $this->apps->copyright . " - " . $this->apps->kd_cabang ;?> &copy; 2017
                        <br>
                        <small>
                            <?php echo $this->apps->dept . ' | Engine Ver : ' . CI_VERSION .' | Server Ver : ' . phpversion();?>
                        </small>
                        <br>
                        <small>
                            Tampilan Terbaik Gunakan 
                            <a style="color:#536c79;text-decoration: underline;" href="https://www.google.com/chrome/browser/desktop/" target="blank">Google Chrome</a> 
                            Terbaru
                        </small>
                    </p>
                </div>
                <?php 
                    echo form_close(); 
                ?>
            </div>
        </div>
    </div>
</div>
