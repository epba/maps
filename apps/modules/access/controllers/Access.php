<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Access extends CI_Controller {

    protected $data;
    
    public function __construct() {
        parent::__construct();
        $this->_init_form();
        $link = $this->uri->segment(2);
        if ($link !== "logout") {
            $logged_in = $this->session->userdata('logged_in');
            if ($logged_in) {
                redirect("main");
            }
        }

        if ($this->agent->is_browser()) {
            $this->data['agent'] = $this->agent->browser() . ' ' . $this->agent->version();
        } elseif ($this->agent->is_robot()) {
            $this->data['agent'] = $this->agent->robot();
        } elseif ($this->agent->is_mobile()) {
            $this->data['agent'] = $this->agent->mobile();
        } else {
            $this->data['agent'] = 'Unidentified User Agent';
        }
    }
	
    public function index() {
        $this->template
                ->title('Login User', $this->apps->name)
                ->set_layout('access')
                ->build('index', $this->data);
    }

    public function login() {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">'
                . ' <button type="button" class="close" data-dismiss="alert">×</button>', '</div>');

        if ($this->form_validation->run() == FALSE) {
            $this->template
                    ->title('Login User', $this->apps->name)
                    ->set_layout('access')
                    ->build('index', $this->data);
        } else {
            $identity = $this->input->post('username');
            $password = $this->input->post('password');
            $remember = $this->input->post('remember');
            if ($identity === "admin" && $password === "admin") {
                $data = array(
                    'username' => $identity,
                    'name' => $identity,
                    'platform' => $this->agent->platform(),
                    'browser' => $this->data['agent'],
                    'logged_in' => true,
                );
        
                $this->session->set_userdata($data);
                $res = array(
                    'state' => '1',
                    'title' => 'Login Berhasil!',
                    'msg' => 'Selamat Datang, ' . $identity,
                    'token' => $this->security->get_csrf_hash(),
                );
            } else {
                $res = array(
                    'state' => '0',
                    'title' => 'Login Gagal!',
                    'msg' => 'Username atau Password anda salah!',
                    'token' => $this->security->get_csrf_hash(),
                );
            }
            echo json_encode($res);
        }
    }

    public function logout() {
        $res = $this->session->sess_destroy();
        if ($res) {
            redirect('access', 'refresh');
        } else {
            redirect('main');
        }
    }

    private function _init_form() {
        $this->data['form'] = array(
            'username' => array(
                'placeholder' => 'Username',
                'type' => 'text',
                'id' => 'username',
                'name' => 'username',
                'value' => set_value('username'),
                'class' => 'form-control',
                'required' => '',
            ),
            'password' => array(
                'placeholder' => 'Password',
                'type' => 'password',
                'id' => 'password',
                'name' => 'password',
                'value' => set_value('password'),
                'class' => 'form-control',
                'required' => '',
            ),
        );
    }

}
