<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.4
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
-->
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Google Maps Web Application">
        <meta name="author" content="Pudyasto Adi Wibowo">
        <meta name="keyword" content="Google Maps, GMaps, PAW!">
        <meta name="csrf" id="<?= $this->security->get_csrf_token_name(); ?>" 
              content="<?= $this->security->get_csrf_hash(); ?>">
        <link rel="icon" href="<?= base_url('assets/img/favicon.png'); ?>" type="image/png">
        
        <title><?php echo $template['title']; ?></title><meta charset="UTF-8" />

        <!-- Icons -->
        <link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/plugins/simple-line-icons/css/simple-line-icons.css'); ?>" />
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?= base_url('assets/plugins/datepicker/datepicker3.css'); ?>">
        <!-- Data Tables -->
        <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables/datatables.min.css'); ?>">
        <!-- Chosen -->
        <link href="<?=base_url('assets/plugins/chosen/bootstrap-chosen.css');?>" rel="stylesheet">
        <!-- toastr -->
        <link rel="stylesheet" href="<?=base_url('assets/plugins/toastr/toastr.min.css');?>">

        <!-- pre-loader -->
        <link href="<?= base_url('assets/plugins/pre-loader/normalize.css'); ?>" rel="stylesheet">
        <link href="<?= base_url('assets/plugins/pre-loader/main.css'); ?>" rel="stylesheet">  
        
        <!-- Select2 -->
        <link href="<?=base_url('assets/plugins/select2/css/select2.css');?>" rel="stylesheet">
        <link href="<?=base_url('assets/plugins/select2/css/AdminLTE-select2.css');?>" rel="stylesheet">
        
        <!-- Duallistbox -->
        <link rel="stylesheet" href="<?= base_url('assets/plugins/duallistbox/bootstrap-duallistbox.css'); ?>">
    


        <link href="<?= base_url('assets/coreui/css/style.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?= base_url('assets/css/my.css'); ?>">
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">    
    
        <div id="loader-wrapper">
            <div id="loader"></div>
            <div class="loader-section"></div>
        </div>

        <header class="app-header navbar">
            <button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
                <span class="navbar-toggler-icon"></span>
            </button>

            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item d-md-down-none">
                    <a class="nav-link date-time" href="#"></a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <?= $this->session->userdata('name'); ?>
                        <i class="fa fa-gear"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header text-center">
                            <strong>Settings</strong>
                        </div>
                        <a class="dropdown-item btn-logout" href="javascript:void(0);"><i class="fa fa-lock"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </header>

        <div class="app-body">
            <div class="sidebar">
                <nav class="sidebar-nav">
                <?php
                    $data = array(
                        array(
                            'id'=>1,
                            'data'=>site_url('main/home'),
                            'label'=>'<i class="icon-home"></i> Home',
                            'title' => 'Halaman beranda'
                        ),
                        array(
                            'id'=>2,
                            'label'=>'<i class="icon-map"></i> Maps',
                            'title' => 'Manajemen map'
                        ),
                        array(
                            'id'=>20,
                            'data'=>site_url('polygon'),
                            'label'=>'Polygon',
                            'title' => 'Menggambar bentuk pada maps'
                        ),
                    );

                    echo menu_tree($data);
                ?>
                </nav>
                <button class="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>

            <!-- Main content -->
            <main class="main">
                <div class="container-fluid" id="main-content">
                    <?php echo $template['body']; ?>
                </div>
                <!-- /.conainer-fluid -->
            </main>
            
            <!-- Form Modal -->
            <div class="modal fade" id="form-modal" role="dialog" data-backdrop="static" aria-labelledby="form-modal" aria-hidden="true">
                <div class="modal-dialog" style="min-width: 700px;" role="document">
                    <div class="modal-content animated fadeInDown">
                        <div class="modal-header">
                            <h4 class="modal-title" id="form-modal-title" style="font-size: 18px;"></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="form-modal-content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="app-footer">
            <span>
                <a href="http://pudyastoadi.my.id"><?=$this->apps->name;?></a> © <?=(date('Y')>2018) ? '2018 - ' . date('Y') : '2018';?>
            </span>
            <span class="ml-auto">
                Powered by <a href="http://coreui.io">CoreUI</a>
            </span>
        </footer>

        <!-- Place your JS Here (Start Section)-->  
        <!-- Bootstrap and necessary plugins -->
        <script src="<?= base_url('assets/coreui/js/jquery-3.2.1.min.js'); ?>"></script> 
        <script src="<?= base_url('assets/plugins/popper.js/umd/popper.min.js'); ?>"></script>   
        <script src="<?= base_url('assets/coreui/js/bootstrap.min.js'); ?>"></script>  
        <script src="<?= base_url('assets/plugins/pace/pace.min.js'); ?>"></script>
        
        <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.all.js'); ?>"></script>  
        <script src="<?= base_url('assets/plugins/numeral/numeral.min.js'); ?>"></script>      
        <link rel="stylesheet" href="<?= base_url('assets/plugins/sweetalert2/sweetalert2.css'); ?>"> 
        
        <!-- datepicker -->
        <script src="<?= base_url('assets/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
        
        <!-- datatables -->
        <script src="<?= base_url('assets/plugins/datatables/datatables.min.js'); ?>"></script>
        <!-- Chosen -->
        <script src="<?=base_url('assets/plugins/chosen/chosen.jquery.js');?>"></script>
        
        <!-- Toast-->
        <script src="<?=base_url('assets/plugins/toastr/toastr.min.js');?>"></script>
    
        <!-- Numeral.Js -->
        <script src="<?=base_url('assets/plugins/numeral/numeral.min.js');?>"></script> 
        
        <!-- Chosen -->
        <script src="<?=base_url('assets/plugins/select2/js/select2.full.min.js');?>"></script>
        
        <!-- Chosen -->
        <script src="<?=base_url('assets/plugins/moment/moment.min.js');?>"></script>
        
        <!--    jquery-form-->
        <script src="<?=base_url('assets/plugins/jquery.form/jquery.form.min.js');?>"></script> 
        
        <!-- Duallistbox -->
        <script src="<?= base_url('assets/plugins/duallistbox/jquery.bootstrap-duallistbox.js'); ?>"></script>
    
        <!-- pre-loader -->
        <script src="<?= base_url('assets/plugins/pre-loader/modernizr-2.6.2.min.js'); ?>"></script>    
        <script src="<?= base_url('assets/plugins/pre-loader/main.js'); ?>"></script>   
        <!-- Plugins and scripts required by all views -->


        <!-- CoreUI main scripts -->
        <script src="<?= base_url('assets/coreui/js/app.js'); ?>"></script>
        <script src="<?= base_url('assets/js/my.js'); ?>"></script>
        <script src="<?= base_url('assets/js/main.js'); ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?=$this->apps->key;?>"></script>
        <!-- Place your JS Here (End Section)-->          
    </body>
</html>
