<!--
 * CoreUI - Open Source Bootstrap Admin Template
 * @version v1.0.4
 * @link http://coreui.io
 * Copyright (c) 2017 creativeLabs Łukasz Holeczek
 * @license MIT
-->
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Google Maps Web Application">
        <meta name="author" content="Pudyasto Adi Wibowo">
        <meta name="keyword" content="Google Maps, GMaps, PAW!">
        <!-- Favicon-->
        <link rel="icon" href="<?= base_url('assets/img/favicon.png'); ?>" type="image/png">

        <title><?php echo $template['title']; ?></title><meta charset="UTF-8" />

        <!-- Icons -->
        <link rel="stylesheet" href="<?= base_url('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css'); ?>" />
        <link rel="stylesheet" href="<?= base_url('assets/plugins/simple-line-icons/css/simple-line-icons.css'); ?>" />


        <!-- Main styles for this application -->
        <link rel="stylesheet" href="<?= base_url('assets/coreui/css/style.css'); ?>">
        <link rel="stylesheet" href="<?= base_url('assets/css/my.css'); ?>">
        <style type="text/css">
            .app {
              min-height: 75vh; 
            }
        </style>
        <!-- Styles required by this views -->

    </head>


    <body class="flex-row align-items-center">
        <div class="container">
            <?php echo $template['body']; ?>
        </div>

        <!-- Bootstrap and necessary plugins -->
        <script src="<?= base_url('assets/coreui/js/jquery-3.2.1.min.js'); ?>"></script> 
        <script src="<?= base_url('assets/plugins/popper.js/umd/popper.min.js'); ?>"></script> 
        <script src="<?= base_url('assets/coreui/js/bootstrap.min.js'); ?>"></script> 
        <script src="<?= base_url('assets/plugins/sweetalert2/sweetalert2.all.js'); ?>"></script>  
        
        <script src="<?= base_url('assets/plugins/numeral/numeral.min.js'); ?>"></script>      
        <link rel="stylesheet" href="<?= base_url('assets/plugins/sweetalert2/sweetalert2.css'); ?>"> 
        <script src="<?= base_url('assets/js/my.js'); ?>"></script>  
        <script src="<?= base_url('assets/js/login.js'); ?>"></script>  
    </body>
</html>
