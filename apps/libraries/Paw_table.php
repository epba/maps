<?php

/*
 * ***************************************************************
 * Script : Paw_table.php 
 * Version : 1.0.0
 * Date : Mar 1, 2018 / 10:38:17 AM
 * Author : Pudyasto
 * Email : mr.pudyasto@gmail.com
 * Description : Simple datatable json generator library for codeiginter 3.X
 * ***************************************************************
 */

/**
 * Description of Paw_table
 *
 * @author Pudyasto
 */
class Paw_table {

    //put your code here
    protected $aColumns;
    protected $sIndexColumn;
    protected $sLimit;
    protected $sTable;
    protected $sOrder;
    protected $sWhere;
    protected $sQuery;
    protected $rResult;
    protected $iTotalDisplayRecords;
    protected $iTotalRecords;

    public function __construct() {
        $this->ci = & get_instance();
    }
    
    public function output($column,  $table, $index = null) {
        $get = $this->ci->input->post();
        if(empty($get)){
            $get = $this->ci->input->get();
        }
        $this->aColumns = $column;
        $this->sTable = $table;
        if($index){
            $this->sIndexColumn = $index;
        }else{
            $this->sIndexColumn = $this->aColumns[0];
        }
        $this->_set_limit($get);
        $this->_set_order($get);
        $this->_set_where($get);
        $this->_set_total_records();
        $this->_set_query();
        
        if(!isset($get['sEcho'])){
            return "Kesalahan, ID Key tidak ada!";
        }
        if(intval($get['sEcho'])>0){
            $output = array(
                "sEcho" => intval($get['sEcho']),
                "iTotalRecords" => $this->iTotalRecords,
                "iTotalDisplayRecords" => $this->iTotalDisplayRecords,
                "aaData" => $this->rResult->result_array()
            );
        }else{
            $output = array(
                "sEcho" => intval($get['sEcho']),
                "iTotalRecords" => 0,
                "iTotalDisplayRecords" => 0,
                "aaData" => array()
            );
        }
        $csrf_hash = $this->ci->security->get_csrf_hash(); 
        $output['csrf_return'] = $csrf_hash;   
        return json_encode($output);
    }

    private function _set_limit($get) {
        if (!empty($get['iDisplayLength']) && $get['iDisplayLength'] != '-1') {
            $this->sLimit = " LIMIT " . $get['iDisplayLength'];
        } 
        
        if (isset( $get['iDisplayStart'] ) && $get['iDisplayLength'] != '-1') {
            if (intval($get['iDisplayStart']) > 0) {
                $this->sLimit = " LIMIT ".intval( $get['iDisplayLength'] )." OFFSET ".
                        intval( $get['iDisplayStart'] );
            }
        }
    }

    private function _set_order($get) {
        if (isset( $get['iSortCol_0'])) {
            $this->sOrder = " ORDER BY  ";
            for ($i = 0; $i < intval($get['iSortingCols']); $i++) {
                if ($get['bSortable_' . intval($get['iSortCol_' . $i])] == "true") {
                    $this->sOrder .= "" . $this->aColumns[intval($get['iSortCol_' . $i])] . " " .
                            ($get['sSortDir_' . $i] === 'asc' ? 'asc' : 'desc') . ", ";
                }
            }

            $this->sOrder = substr_replace($this->sOrder, "", -2);
            if ($this->sOrder == " ORDER BY") {
                $this->sOrder = "";
            }
        }
    }

    private function _set_where($get) {
        if (isset($get['sSearch']) && $get['sSearch'] != "") {
            $this->sWhere = " Where (";
            for ($i = 0; $i < count($this->aColumns); $i++) {
                $this->sWhere .= "lower(" . $this->aColumns[$i] . ") LIKE '%"
                        . strtolower($this->ci->db->escape_str($get['sSearch'])) . "%' OR ";
            }
            $this->sWhere = substr_replace($this->sWhere, "", -3);
            $this->sWhere .= ')';
        }

        for ($i = 0; $i < count($this->aColumns); $i++) {
            if (isset($get['bSearchable_' . $i]) && $get['bSearchable_' . $i] == "true" && $get['sSearch_' . $i] != '') {
                if ($this->sWhere == "") {
                    $this->sWhere = " WHERE ";
                } else {
                    $this->sWhere .= " AND ";
                }
                $this->sWhere .= "lower(" . $this->aColumns[$i] . ")  LIKE '%"
                        . strtolower($this->ci->db->escape_str($get['sSearch_' . $i])) . "%' ";
            }
        }
    }
    
    private function _set_total_records() {
        $q = "SELECT COUNT("
                . $this->sIndexColumn
                . ") res "
                . " FROM "
                . $this->sTable
                . $this->sWhere;

        $res = $this->ci->db->query($q);
        if($res->num_rows()>0){
            $data = $res->result_array();
            $this->iTotalDisplayRecords = $data[0]['res'];
        }
    }

    private function _set_query() {
        $this->sQuery = "SELECT "
                . str_replace(" , ", " ", implode(", ", $this->aColumns))
                . " FROM "
                . $this->sTable
                . $this->sWhere
                . $this->sOrder
                . $this->sLimit;
        $this->rResult = $this->ci->db->query($this->sQuery);
        $this->iTotalRecords = $this->rResult->num_rows();
    }

}
