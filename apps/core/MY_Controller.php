<?php

/*
 * ***************************************************************
 * Script : 
 * Version : 
 * Date :
 * Author : Pudyasto Adi W.
 * Email : mr.pudyasto@gmail.com
 * Description : 
 * ***************************************************************
 */

/**
 * Description of MY_Controller
 *
 * @author pudyasto
 */
class MY_Controller extends CI_Controller{  
    var $menu_app="";
    var $msg_active="";
    var $msg_main="";
    var $msg_detail="";
    var $csrf="";
    
    public function __construct()
    {
        parent::__construct();  

        $logged_in = $this->session->userdata('logged_in');
        
        if(!$logged_in){
            redirect($this->apps->ssoapp . '/access?url=' . $this->apps->curPageURL(),'refresh');
            exit;
        }
 
        
        header("cache-Control: no-store, no-cache, must-revalidate");
        header("cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
        
    }
}
