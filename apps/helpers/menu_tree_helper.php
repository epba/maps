<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('menu_tree'))
{      
    function menu_tree($array, $parent=0, $curr=0, $prev=-1)
    {
        $result = "";
        foreach($array as $value)
        {    
            if(strlen($value['id'])==1)
            {
                $xparent=0;
            }
            else
            {
                $xparent=substr($value['id'], 0,strlen($value['id'])-1);
            }
            if($parent==$xparent)
            {
                if($curr>$prev)
                {
                    if($xparent==0)
                    {
                        $result.= '<ul class="nav">';
                    }
                    else
                    {
                        $result.= '<ul class="nav-dropdown-items" id="menu_'.$xparent.'">';
                    }
                }
                if($curr=$prev)
                {
                    $result.= '</li>';
                }
                if(!isset($value['data']) || $value['data']=='')
                {
                    $result.= '<li class="nav-item nav-dropdown"><a href="#" class="nav-link nav-dropdown-toggle nav-main-menu" title="'.$value['title'].'"';
                }
                else
                {
                    $result.= '<li class="nav-item" ><a class="nav-link nav-menu" href="'.$value['data'].'" title="'.$value['title'].'" ';
                }
                $result.= '>'.$value['label'];
                $result.= '</a>';
                if($curr>$prev)
                {
                    $prev=$curr;
                }  
                $curr++;
                $result.= menu_tree($array,$value['id'],$curr,$prev);
                $curr--;
            }
        }
        if($curr==$prev)
        {
            $result.= "</li></ul>";
        }

        return $result;
    }
}
/* 
 * Created by titounnes - https://github.com/titounnes/menu-recursive
 * Optimize by Pudyasto - https://github.com/pudyasto
 */

